let userDate = null;
let date = null;
let year =null;
let month = null;
let day =null;
let age =null;
const signAstrological ={
    "Aries":      [new Date(1900,2,21), new Date(1900,3,20), "\u2648"],
    "Taurus":     [new Date(1900,3,21), new Date(1900,4,20),"\u2648"],
    "Gemini":     [new Date(1900,4,21), new Date(1900,5,21),"\u264A"],
    "Cancer":     [new Date(1900,5,22), new Date(1900,6,22),"\u264B"],
    "Leo":        [new Date(1900,6,23), new Date(1900,7,23),"\u264C"],
    "Virgo":      [new Date(1900,7,24), new Date(1900,8,23),"\u264D"],
    "Libra":      [new Date(1900,8,24), new Date(1900,9,23),"\u264E"],
    "Scorpio":    [new Date(1900,9,24), new Date(1900,10,22),"\u264F"],
    "Sagittarius":[new Date(1900,10,23), new Date(1900,11,21),"\u2650"],
    "Capricorn":  [new Date(1900,11,22), new Date(1901,0,20),"\u2651"],
    "Aquarius":   [new Date(1900,0,21), new Date(1900,1,20),"\u2652"],
    "Pisces":     [new Date(1900,1,21), new Date(1900,2,20),"\u2653"]
};
const animalSign =['Horse', 'Sheep', 'Monkey','Chicken','Dog','Pig','Rat','Ox','Tiger','Rabbit','Dragon', 'Snake'];

function validate(date) {
    while (true){
        day = date.slice(0,2);
        month = date.slice(3,5);
        year = date.slice(6);
        if (date !== (day+"."+month+"."+year) || isNaN(+year) || isNaN(+month) || isNaN(+day) || +year < 1900 || +year >((new Date).getFullYear())-1 || +month>12 || +month< 1 || +day >31 || +day < 1|| date.length!==10){
           date = prompt(`It isn't correct format. Enter your Date of Birth in format dd.mm.yyyy`);
        }
        else {
        break;
        }
    }
    return date;
}

function getAstrological() {
    userDate = prompt('Enter your Date of Birth in format dd.mm.yyyy');
    userDate = validate(userDate);
    const dateArr = userDate.split('.');
    if ( dateArr[1] == 1 && dateArr[0]< 21 ) {
        date = new Date(1901, dateArr[1]-1,dateArr[0]);
    }
    else{
        date = new Date(1900, dateArr[1]-1,dateArr[0]);
    }
    age = parseInt((new Date - new Date(dateArr[2],dateArr[1],dateArr[0]))/(1000*60*60*24*365));
    alert (`Are your age is ${age}?`);
    for (let key in signAstrological) {
        if (date >= signAstrological[key][0] && date <= signAstrological[key][1]){
            alert (`Your Astrological sign is ${key} ${signAstrological[key][2]}` );
        }
    }
    switch ((dateArr[2]-1918) % 12) {
        case 0 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[0]}`);
            break;
        case 1 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[1]}`);
            break;
        case 2 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[2]}`);
            break;
        case 3 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[3]}`);
            break;
        case 4 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[4]}`);
            break;
        case 5 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[5]}`);
            break;
        case 6 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[6]}`);
            break;
        case 7 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[7]}`);
            break;
        case 8 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[8]}`);
            break;
        case 9 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[9]}`);
            break;
        case 10 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[10]}`);
            break;
        case 11 : alert(`Your year of the animal on the Chinese calendar is year of the ${animalSign[11]}`);
            break;
    }
}
getAstrological();
